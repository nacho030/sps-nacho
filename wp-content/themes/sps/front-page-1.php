    <?php get_header(); ?> 
    <!-- Contact Form Section -->
    <div class="container contact-section">
        <div class="form-contact container">
            <div class="col-1 col-md-4 col-lg-6"></div>
            <div class="col-10 col-md-6 col-lg-4 form-contact-section">
                <?php echo do_shortcode( '[ninja_form id=1]' ); ?>
            </div>
            <div class="col-1 col-md-2 col-lg-2"></div>
        </div>
        <div class="row contact-section-1">
            <div class="col-12 col-sm-8 col-md-9 col-lg-8 contact-section-img"><img class="img-banner-contact" src="<?php echo get_template_directory_uri(); ?>/img/AdobeStock_236073824.jpg"></div>
            <div class="col-1 col-sm-4 col-md-3 col-lg-4 contact-section-background"></div>
        </div>
    </div>
    <div class="container contact-section-bottom"></div>

    <!-- Contact Section -->
    <div class="container icon-section-title">
        <div class="row">
            <div class="col-md-2 col-lg-4 block-section"></div>
            <div class="col-12 col-md-8 col-lg-4">
                <h1 class="title-contact-2">Queremos ayudarte</h1>
                <h2 class="title-contact-1">A encontrar tu tranquilidad</h2>
                <h3 class="subtitle-white subtitle-white-contact">Dejanos tus datos</h3>
                <a href="#" class="btn btn-contact-2">Contacto</a>
            </div>
            <div class="col-md-2 col-lg-4 block-section"></div>
        </div>    
    </div>

    
    <!-- Map Section -->
    <div class="container map-section">
        <div class="row">
            <div class="col-12">
                <div class="embed-responsive embed-responsive-16by9 map-section-iframe">
                    <iframe class="embed-responsive-item" src="https://maps.google.com/maps?q=university%20of%20san%20francisco&t=&z=13&ie=UTF8&iwloc=&output=embed" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>

    <?php get_footer(); ?>