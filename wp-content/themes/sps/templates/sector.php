<?php /* Template Name: Sector */ ?>

<?php get_header(); ?>

<!-- Start Sector template parts-->

<!-- End Sector template parts-->

<!---->
    <!-- Banner Section -->
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="<?php the_field('image_banner_sector'); ?>"  alt="First slide">
                <div class="text-banner-sector">
                    <h3 class="title-banner-sector"><?php the_field('title_banner_sector'); ?></h3>
                    <p class="text-banner-sector-section text-banner-extra-bold"><?php the_field('subtitle_banner_sector'); ?></p>
                </div>
                <div class="carousel-caption carousel-caption-text-sector"></div>
            </div>
        </div>
    </div>

    <!-- Section Sector 1 -->
    <div class="container about-us">
        <div class="row">
            <div class="col-md-1 col-lg-3 block-section"></div>
            <div class="col-12 col-md-10 col-lg-6">
                <p class="text-section-sector-1">
                    <?php the_field('text_last_banner'); ?>
                </p>
            </div>
            <div class="col-md-1 col-lg-3 block-section"></div>
        </div>    
    </div>

    <!-- Icon Sector Section -->
    <div class="container icon-section-sector">
        <div class="row">
            <div class="col-md-2 block-section"></div>
            <div class="col-12 col-md-8 block-section-icon">
                <div class="row row-icon-section" style="margin-top: 4rem; margin-bottom: 2rem;">
                    <div class="col-1 col-md-1 col-lg-1 separate-icon"></div>
                    <div class="col-5 col-md-2 col-lg-2">
                        <div class=""><img class="icon-sector" src="<?php the_field('icon_1_sector'); ?>" alt="logo"></div>
                        <p class="text-icon-section-sector"><?php the_field('icon_text_1_sector'); ?></p>
                    </div>
                    <div class="col-1 col-md-2 col-lg-2"></div>
                    <div class="col-5 col-md-2 col-lg-2">
                        <div class=""><img class="icon-sector" src="<?php the_field('icon_2_sector'); ?>" alt="logo"></div>
                        <p class="text-icon-section-sector"><?php the_field('icon_text_2_sector'); ?></p>
                    </div>
                    <div class="col-1 col-md-2 col-lg-2 separate-icon"></div>
                    <div class="col-5 col-md-2 col-lg-2">
                        <div class=""><img class="icon-sector" src="<?php the_field('icon_3_sector'); ?>" alt="logo"></div>
                        <p class="text-icon-section-sector"><?php the_field('icon_text_3_sector'); ?></p>
                    </div>
                    <div class="col-1 col-md-1 col-lg-1 separate-icon-2"></div>
                    <div class="col-1 col-md-1 col-lg-1"></div>
                    <div class="col-5 col-md-2 col-lg-2">
                        <div class=""><img class="icon-sector" src="<?php the_field('icon_4_sector'); ?>" alt="logo"></div>
                        <p class="text-icon-section-sector"><?php the_field('icon_text_4_sector'); ?></p>
                    </div>
                    <div class="col-1 col-md-2 col-lg-2 separate-icon"></div>
                    <div class="col-5 col-md-2 col-lg-2">
                        <div class=""><img class="icon-sector" src="<?php the_field('icon_5_sector'); ?>" alt="logo"></div>
                        <p class="text-icon-section-sector"><?php the_field('icon_text_5_sector'); ?></p>
                    </div>
                    <div class="col-1 col-md-2 col-lg-2"></div>
                    <div class="col-5 col-md-2 col-lg-2">
                        <div class=""><img class="icon-sector" src="<?php the_field('icon_6_sector'); ?>" alt="logo"></div>
                        <p class="text-icon-section-sector"><?php the_field('icon_text_6_sector'); ?></p>
                    </div>
                    <div class="col-1 col-md-1 col-lg-1 separate-icon"></div>
                    <div class="col-1 col-md-1 col-lg-1"></div>
                    <div class="col-5 col-md-2 col-lg-2">
                        <div class=""><img class="icon-sector" src="<?php the_field('icon_7_sector'); ?>" alt="logo"></div>
                        <p class="text-icon-section-sector"><?php the_field('icon_text_7_sector'); ?></p>
                    </div>
                    <div class="col-1 col-md-2 col-lg-2"></div>
                    <div class="col-5 col-md-2 col-lg-2">
                        <div class=""><img class="icon-sector" src="<?php the_field('icon_8_sector'); ?>" alt="logo"></div>
                        <p class="text-icon-section-sector"><?php the_field('icon_text_8_sector'); ?></p>
                    </div>
                    <div class="col-1 col-md-2 col-lg-2"></div>
                    <div class="col-5 col-md-2 col-lg-2">
                        <div class=""><img class="icon-sector" src="<?php the_field('icon_9_sector'); ?>" alt="logo"></div>
                        <p class="text-icon-section-sector"><?php the_field('icon_text_9_sector'); ?></p>
                    </div>
                    <div class="col-1 col-md-1 col-lg-1 separate-icon"></div>
                    <div class="col-1 col-md-1 col-lg-1"></div>
                    <div class="col-5 col-md-2 col-lg-2 separate-icon"></div>
                    <div class="col-1 col-md-2 col-lg-2 separate-icon"></div>
                    <div class="col-5 col-md-2 col-lg-2">
                        <div class=""><img class="icon-sector" src="<?php the_field('icon_10_sector'); ?>" alt="logo"></div>
                        <p class="text-icon-section-sector"><?php the_field('icon_text_10_sector'); ?></p>
                    </div>
                    <div class="col-1 col-md-2 col-lg-2"></div>
                    <div class="col-5 col-md-2 col-lg-2">
                        <div class=""><img class="icon-sector" src="<?php the_field('icon_11_sector'); ?>" alt="logo"></div>
                        <p class="text-icon-section-sector"><?php the_field('icon_text_11_sector'); ?></p>
                    </div>
                </div>
            </div>
            <div class="col-md-2 block-section"></div>
        </div>    
    </div>

    <!-- Our Principles Section-->
    <div class="container our-principles-section">
        <div class="img-security"><img src="<?php echo get_template_directory_uri(); ?>/img/oficial_isolated.png"></div>
        <div class="background-our-principles"></div>
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-12 col-md-8 col-lg-6 list-our-principles">
                <h1 class="title-black title-black-op"><?php the_field('title_our_principles'); ?></h1>
                <ol class="list-op" type="disc">
                    <?php the_field('text_list_our_principles'); ?>
                </ol>
                <a href="<?php the_field('button_link_our_principles'); ?>" class="btn btn-op"><?php the_field('button_name_our_principles'); ?></a>
            </div>
            <div class="col-md-4 col-lg-3"></div>
        </div>
    </div>

    <!-- Solutions Section Title -->
    <div class="container icon-section-title">
        <div class="row">
            <div class="col-md-3 col-lg-4 block-section"></div>
            <div class="col-12 col-md-6 col-lg-4">
                <h1 class="title-white title-white-sector"><?php the_field('title_solution'); ?></h1>
                <h3 class="subtitle-white subtitle-white-sector"><?php the_field('subtitle_solution'); ?></h3>
            </div>
            <div class="col-md-3 col-lg-4 block-section"></div>
        </div>    
    </div>

    <!-- Solutions Section Content -->
    <div class="container icon-section">
        <div class="row">
            <div class="col-md-2 block-section"></div>
            <div class="col-12 col-md-8 block-section-icon text-solutions-main">
                <div class="text-solutions">
                    <p>
                        <?php the_field('text_solution'); ?>
                    </p>
                    <p>
                        <?php the_field('text_solution_2'); ?>
                    </p>
                </div>
            </div>
            <div class="col-md-2 block-section"></div>
        </div>    
    </div>

    <!-- Profiles Section -->
    <div class="container sector-profiles">
        <div class="sector-industries-color"></div>
        <div class="row">
            <div class=" col-sm-1 col-md-1 col-lg-3 block-section"></div>
            <div class="col-12 col-sm-12 col-md-10 col-lg-6">
                <h1 class="title-white title-white-profile"><?php the_field('title_profile'); ?></h1>
                <div class="container container-sector-industries">
                    <div class="row">
                        <div class="col-12 col-md-4 block-section-profile">
                            <img  src="<?php the_field('image_1_profile'); ?>">
                            <div class="text-profiles">
                                <h2><?php the_field('title_1_profile'); ?></h2>
                                <p><?php the_field('text_1_profile'); ?></p>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 block-section-profile">
                            <img  src="<?php the_field('image_2_profile'); ?>">
                            <div class="text-profiles">
                                <h2><?php the_field('title_2_profile'); ?></h2>
                                <p><?php the_field('text_2_profile'); ?></p>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 block-section-profile">
                            <img  src="<?php the_field('image_3_profile'); ?>">
                            <div class="text-profiles">
                                <h2><?php the_field('title_3_profile'); ?></h2>
                                <p><?php the_field('text_3_profile'); ?></p>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
            <div class="col-sm-1 col-md-1 col-lg-3 block-section"></div>
        </div>    
    </div>

    <!-- Recommendations Section Title -->
    <div class="container icon-section-title">
        <div class="row">
            <div class="col-md-4 block-section"></div>
            <div class="col-12 col-md-4">
                <h1 class="title-white title-white-recommendations" style="margin-top: 2rem;"><?php the_field('title_recommendations'); ?></h1>
            </div>
            <div class="col-md-4 block-section"></div>
        </div>    
    </div>

    <!-- Recommendations Section Content -->
    <div class="container icon-section">
        <div class="row">
            <div class="col-md-1 block-section"></div>
            <div class="col-12 col-md-10 block-section-icon">
                <div class="row row-icon-section row-icon-section-recommendations">
                    <div class="col-1 block-section-2"></div>
                    <div class="col-10 col-md-5 col-lg-5 icon-primary icon-primary-block icon-primary-recommendations-1">
                        <div class="icon-secundary-recommendations"><img class="" src="<?php the_field('icon_1_recommendations'); ?>" alt="logo"></div>
                        <ol class="text-icon-section-recommendations">
                            <?php the_field('text_1_recommendations'); ?>
                        </ol>
                    </div>
                    <div class="col-1 block-section-2"></div>
                    <div class="col-1 block-section-2"></div>
                    <div class="col-10 col-md-5 col-lg-5 icon-primary icon-primary-block icon-primary-recommendations-2">
                        <div class="icon-secundary-recommendations"><img class="" src="<?php the_field('icon_2_recommendations'); ?>" alt="logo"></div>
                        <p class="text-icon-section-recommendations">
                            <?php the_field('text_2_recommendations'); ?>
                        </p>
                    </div>
                    <div class="col-1 block-section-2"></div>
                </div>
            </div>
            <div class="col-md-1 block-section"></div>
        </div>    
    </div>

    <!-- Appoappointment Section -->
    <div class="container appointment-section">
        <div class="row">
            <div class="col-1 col-sm-2 col-md-2 col-lg-3"></div>
            <div class="col-12 col-sm-8 col-md-8 col-lg-6">
                <h1 class="title-appointment-1"><?php the_field('title_before_footer'); ?></h1>
                <h2 class="title-appointment-2"><?php the_field('subtitle_before_footer'); ?></h2>
                <a href="<?php the_field('button_link_before_footer'); ?>" class="btn btn-appointment"><?php the_field('button_name_before_footer'); ?></a>
            </div>
            <div class="col-1 col-sm-2 col-md-2 col-lg-3"></div>
        </div>
    </div>
<!---->

<?php get_footer(); ?>
