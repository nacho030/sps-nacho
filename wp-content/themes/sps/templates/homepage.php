<?php /* Template Name: Homepage */ ?>

<?php get_header(); ?>

<!-- Start Homepage template parts-->
<?php get_template_part('template-parts/homepage/banner'); ?>
<!-- End Homepage template parts-->

<!---->
<!-- Banner Section -->
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <?php 
                    $image = get_field('image_banner');
                    if(!empty($image)): 
                ?>
                    <img class="d-block w-100" src="<?php echo $image['url']; ?>"  alt="<?php echo $image['alt']; ?>">
                <?php endif; ?>
                <div class="carousel-caption carousel-caption-text">
                    <h3 class="title-banner"><?php the_field('title_banner'); ?></h3>
                    <p class="text-banner text-banner-light"><?php the_field('subtitle_banner_1'); ?></p>
                    <p class="text-banner text-banner-extra-bold"><?php the_field('subtitle_banner_2'); ?></p>
                </div>
            </div>
            <!--<div class="carousel-item">
                <img class="d-block w-100" src="<?php echo get_template_directory_uri(); ?>/img/banner1.jpg" alt="Second slide">
                <div class="carousel-caption carousel-caption-text">
                    <h3 class="title-banner">SPS SECURITY PROTECTORS</h3>
                    <p class="text-banner">CONOCIDOS POR OPERAR BAJO</p>
                    <p class="text-banner"><strong>LA CONSISTENCIA Y LA DISCIPLINA</strong></p>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="<?php echo get_template_directory_uri(); ?>/img/banner1.jpg" alt="Third slide">
                <div class="carousel-caption carousel-caption-text">
                    <h3 class="title-banner">SPS SECURITY PROTECTORS</h3>
                    <p class="text-banner">CONOCIDOS POR OPERAR BAJO</p>
                    <p class="text-banner"><strong>LA CONSISTENCIA Y LA DISCIPLINA</strong></p>
                </div>
            </div>-->
        </div>
    </div>

    <!-- About us Section -->
    <div class="container about-us">
        <div class="row">
            <div class="col-md-3 block-section"></div>
            <div class="col-12 col-md-6">
                <h1 class="title-black"><?php the_field('about_us_title'); ?></h1>
                <p class="text-about-us">
                    <?php the_field('about_us_text'); ?>
                </p>
            </div>
            <div class="col-md-3 block-section"></div>
        </div>    
    </div>

    <!-- Human Talent Section -->
    <div class="container human-talent">
        <div class="row">
            <div class="col-md-3 block-section"></div>
            <div class="col-12 col-md-6">
                <h1 class="title-black"><?php the_field('our_footprint_title'); ?></h1>
                <h3 class="subtitle-black"><?php the_field('our_footprint_subtitle'); ?></h3>
                <div class="embed-responsive embed-responsive-16by9 embed-responsive-iframe">
                    <iframe width="560" height="315" src="<?php the_field('video_link'); ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-md-3 block-section"></div>
        </div>    
    </div>

    <!-- Icons Section 1 -->
    <div class="container icon-section-title">
        <div class="row">
            <div class="col-md-4 block-section"></div>
            <div class="col-12 col-md-4">
                <h1 class="title-white" style="margin-top: 2rem;"><?php the_field('icon_title_section'); ?></h1>
                <h3 class="subtitle-white"><?php the_field('icon_subtitle_section'); ?></h3>
            </div>
            <div class="col-md-4 block-section"></div>
        </div>    
    </div>

    <!-- Icons Section 2 -->
    <div class="container icon-section">
        <div class="row">
            <div class="col-md-2 block-section"></div>
            <div class="col-12 col-md-8 block-section-icon">
                <div class="row row-icon-section" style="margin-top: 4rem; margin-bottom: 2rem;">
                    <div class="col-1 col-md-1 col-lg-1 separate-icon"></div>
                    <div class="col-5 col-md-2 col-lg-2 icon-primary icon-primary-block">
                        <div class="icon-secundary"><img class="" src="<?php the_field('icon_1'); ?>" alt="logo"></div>
                        <p class="text-icon-section"><?php the_field('icon_1_text'); ?></p>
                    </div>
                    <div class="col-1 col-md-2 col-lg-2"></div>
                    <div class="col-5 col-md-2 col-lg-2 icon-primary icon-primary-block">
                        <div class="icon-secundary"><img class="" src="<?php the_field('icon_2'); ?>" alt="logo"></div>
                        <p class="text-icon-section"><?php the_field('icon_2_text'); ?></p>
                    </div>
                    <div class="col-1 col-md-2 col-lg-2 separate-icon"></div>
                    <div class="col-5 col-md-2 col-lg-2 icon-primary icon-primary-block">
                        <div class="icon-secundary"><img class="" src="<?php the_field('icon_3'); ?>" alt="logo"></div>
                        <p class="text-icon-section"><?php the_field('icon_3_text'); ?></p>
                    </div>
                    <div class="col-1 col-md-1 col-lg-1 separate-icon-2"></div>
                    <div class="col-1 col-md-1 col-lg-1"></div>
                    <div class="col-5 col-md-2 col-lg-2 icon-primary icon-primary-block">
                        <div class="icon-secundary"><img class="" src="<?php the_field('icon_4'); ?>" alt="logo"></div>
                        <p class="text-icon-section"><?php the_field('icon_4_text'); ?></p>
                    </div>
                    <div class="col-1 col-md-2 col-lg-2 separate-icon"></div>
                    <div class="col-5 col-md-2 col-lg-2 icon-primary icon-primary-block">
                        <div class="icon-secundary"><img class="" src="<?php the_field('icon_5'); ?>" alt="logo"></div>
                        <p class="text-icon-section"><?php the_field('icon_5_text'); ?></p>
                    </div>
                    <div class="col-1 col-md-2 col-lg-2"></div>
                    <div class="col-5 col-md-2 col-lg-2 icon-primary icon-primary-block">
                        <div class="icon-secundary"><img class="" src="<?php the_field('icon_6'); ?>" alt="logo"></div>
                        <p class="text-icon-section"><?php the_field('icon_6_text'); ?></p>
                    </div>
                    <div class="col-1 col-md-1 col-lg-1 separate-icon"></div>
                </div>
                <div class="row" style="margin-top: 2rem; margin-bottom: 4rem;">
                    <div class="col-3 col-md-4 icon-central"></div>
                    <div class="col-6 col-md-4 icon-central icon-primary icon-primary-block">
                        <div class="icon-secundary-central"><img class="" src="<?php the_field('icon_7'); ?>" alt="logo"></div>
                        <p class="text-icon-section"><?php the_field('icon_7_text'); ?></p>
                    </div>
                    <div class="col-3 col-md-4 icon-central"></div>
                </div>
            </div>
            <div class="col-md-2 block-section"></div>
        </div>    
    </div>

    <!-- Sectors and Industries Section -->
    <div class="container sector-industries" style="background-image: linear-gradient(
        rgba(11, 39, 73, 0.5),
        rgba(11, 39, 73, 0.5)
      ), url('<?php the_field('background-image'); ?>');">
        <div class="sector-industries-color"></div>
        <div class="row">
            <div class="col-md-3 block-section"></div>
            <div class="col-12 col-md-6">
                <h1 class="title-white"><?php the_field('sectors_and_industries_title'); ?></h1>
                <div class="container container-sector-industries">
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <h3 class="title-sector-industries"><?php the_field('sub_section_1_title'); ?></h3>
                            <p class="text-sector-industries">
                                <?php the_field('sub_section_1_text'); ?>
                            </p>
                            <a href="<?php the_field('button_link_section_1'); ?>"  class="btn btn-sector-industries"><?php the_field('button_name_section_1'); ?></a>
                        </div>
                        <div class="col-12 col-md-4">
                            <h3 class="title-sector-industries"><?php the_field('sub_section_2_title'); ?></h3>
                            <p class="text-sector-industries">
                                <?php the_field('sub_section_2_text'); ?>
                            </p>
                            <a href="<?php the_field('button_link_section_2'); ?>" class="btn btn-sector-industries"><?php the_field('button_name_section_2'); ?></a>
                        </div>
                        <div class="col-12 col-md-4">
                            <h3 class="title-sector-industries"><?php the_field('sub_section_3_title'); ?></h3>
                            <p class="text-sector-industries">
                                <?php the_field('sub_section_3_text'); ?>
                            </p>
                            <a href="<?php the_field('button_link_section_3'); ?>" class="btn btn-sector-industries"><?php the_field('button_name_section_3'); ?></a>
                        </div>
                    </div>
                </div>
               
            </div>
            <div class="col-md-3 block-section"></div>
        </div>    
    </div>
<!---->

<?php get_footer(); ?>
