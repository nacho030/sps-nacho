<!DOCTYPE html>
<html <?php language_attributes();?>>

<head>
	<meta charset="<?php bloginfo('charset');?>">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
	<?php
		if(file_exists(stream_resolve_include_path('gtm-head.php'))) {
			require_once('gtm-head.php');
		}
	?>
</head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<body>
    <?php
        if(file_exists(stream_resolve_include_path('gtm-body.php'))) {
            require_once('gtm-body.php'); 
        }
    ?>

	<?php get_template_part('template-parts/commons/header'); ?>
