var gulp = require('gulp'),
    sass = require('gulp-sass'),
    cleanCSS = require('gulp-clean-css');
    rename = require('gulp-rename'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify')
    imagemin = require('gulp-imagemin'),
    livereload = require('gulp-livereload'),
    jshint = require('gulp-jshint'),
    plumber = require('gulp-plumber'),
    eyeglass = require('eyeglass');

var source = 'assets/';
var dest = 'dist/';

var boostrapSassSrc = './node_modules/bootstrap/';
var slickSassSrc = './node_modules/slick-carousel/';

// Fonts, including bootstrap fonts
var fonts = {
    in: [ source +  'fonts/**/*', boostrapSassSrc + 'assets/fonts/**/*', slickSassSrc + 'slick/fonts/*' ],
    out: dest + 'css/fonts/'
};

// Fonts, including bootstrap fonts
var gifSlick = {
    in: [ slickSassSrc + 'slick/ajax-loader.gif' ],
    out: dest + 'css/'
};


/**
 * Fonts :
 * copy to dist, with Bootstrap fonts too
 *
 */
gulp.task('fonts', function () {
    return gulp
        .src( fonts.in )
        .pipe( gulp.dest( fonts.out ) );
});

gulp.task('loader', function () {
    return gulp
        .src( gifSlick.in )
        .pipe( gulp.dest( gifSlick.out ) );
});


/**
 * Style :
 * Sass compilation including bootstrap-sass
 *
 */
gulp.task('sass', function() {
    return gulp.src( source + '/scss/theme.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
          browsers: ['last 6 versions'],
          cascade: false
        }))
        .pipe(cleanCSS({
          compatibility: 'ie8'
        }))
        .pipe( rename('theme.css') )
        .pipe( gulp.dest( dest + 'css' ) );
});

/**
 * Js lint of main.js
 * stop gulp task on error
 *
 */
gulp.task('lint', function () {
  return gulp.src( source + '/js/main.js' )
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'));
});

/**
 * Javascript
 *
 */

gulp.task('js', function() {
    // Js librairies used on current theme
    gulp.src( [ slickSassSrc + 'slick/slick.min.js' ] )
        .pipe( concat( 'libs.js' ))
        .pipe( gulp.dest( dest + 'js/' ) );

    // Theme main js file
    return gulp.src( source + '/js/*.js' )
        .pipe( concat( 'main.js' ))
        .pipe( plumber() )
        .pipe( uglify() )
        .pipe( gulp.dest( dest + 'js/' ) );
});




/**
 * Image compression
 *
 */
gulp.task( 'img', function() {
    return gulp.src( source + '/img/*.{png,jpg,gif}' )
        //.pipe( imagemin() )
        .pipe( gulp.dest( dest + 'images/' ) );
});

/**
 * Watch task
 *
 */
gulp.task( 'watch', function() {
    gulp.watch( 'assets/scss/**/*.scss', ['sass'] );
    gulp.watch( 'assets/js/**/*.js', ['js'] );
    gulp.watch( 'assets/images/**/*', ['img'] );    
});
