<header class="site-header container">
    <div class="container-site row">
        <div class="nav-header col-4 col-md-4 header-1">
            <div class="logo">
                <img class="header-logo" src="<?php the_field('header_logo', 'option'); ?>" alt="logo">
            </div>
        </div>

        <div class="nav-header col-2 header-2"></div>
        
        <?php 
            $args = array(
                'theme_location'	=> 'main_menu',
                'container'      	=> 'nav',
                'container_class'	=> 'menu_principal col-6 col-md-8 header-3'
            );

            wp_nav_menu($args);
        ?>
    </div>

</header>