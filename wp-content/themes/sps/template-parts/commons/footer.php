<div class="whatsaap-block">
    <a href="https://wa.me/<?php the_field('whatsaap_number', 'option'); ?>"><i class="fab fa-whatsapp icon-whatsapp"></i> Hablénos</a>
    <a href="#" style="margin-left: 4rem"><i class="fas fa-angle-up" style="font-size: 2rem;"></i></a>
</div>
<footer class="site-footer container">
    <div class="row">
        <div class="col-12 site-footer-content">
            <div class="content-footer">
                <a href="<?php the_field('button_link_footer', 'option'); ?>" class="btn btn-contact"><?php the_field('footer_button', 'option'); ?></a>
                <p style="color: #fff"><?php the_field('footer_location', 'option'); ?></p>
                <p style="color: #fff"><a href="mailto:<?php the_field('footer_email', 'option'); ?>"><?php the_field('footer_email', 'option'); ?></a></p>
                <p style="color: #fff"><a href="tel:<?php the_field('footer_phone', 'option'); ?>"><?php the_field('footer_phone', 'option'); ?></a></p>
                <div class="row">
                    <div class="col-4"></div>
                    <div class="col- 4 row social-networks">
                        <div class="col-4"><a href="<?php the_field('facebook_link', 'option'); ?>"><img class="social-icon" src="<?php echo get_template_directory_uri(); ?>/img/facebook.png" alt="facebook"></a></div>
                        <div class="col-4"><a href="<?php the_field('twiter_link', 'option'); ?>"><img class="social-icon" src="<?php echo get_template_directory_uri(); ?>/img/twiter.png" alt="twiter"></a></div>
                        <div class="col-4"><a href="<?php the_field('linkedin_link', 'option'); ?>"><img class="social-icon" src="<?php echo get_template_directory_uri(); ?>/img/linkedin.png" alt="linkedin"></a></div>
                    </div>
                    <div class="col-4"></div>
                </div>
                <p class="copyright-text">@ <?php echo date('Y'); ?> <?php the_field('footer_copyright', 'option'); ?>. TODOS LOS DERECHOS RESERVADOS</p>
            </div>
            
        </div>
    </div>
</footer>