<!DOCTYPE html>
<html <?php language_attributes();?>>

<head>
	<meta charset="<?php bloginfo('charset');?>">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
	<?php
		if(file_exists(stream_resolve_include_path('gtm-head.php'))) {
			require_once('gtm-head.php');
		}
	?>
</head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<body>
    <?php
        if(file_exists(stream_resolve_include_path('gtm-body.php'))) {
            require_once('gtm-body.php'); 
        }
    ?>

	<nav class="navbar navbar-expand-md bg-dark navbar-dark">
		<div class="container">
			<a class="navbar-brand" href="#"><img width="100" src="<?php echo get_template_directory_uri(); ?>/img/SPS_logo horizontal_b.png"></a>
		
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
				<span class="navbar-toggler-icon"></span>
			</button>
			<h1>hola</h1>
			

			

			<div class="collapse navbar-collapse" id="navbar-content">
				<?php
					wp_nav_menu( array(
						'menu'           => 'Main menu',
						'theme_location' => 'main_menu', // Defined when registering the menu
						'menu_id'        => 'main_menu',
						'container'      => 'div',
						'container_class' => 'collapse navbar-collapse',
						'depth'          => 2,
						'menu_class'     => 'navbar-nav mrs-auto',
						'walker'         => new bootstrap_navwalker(), // This controls the display of the Bootstrap Navbar
						'fallback_cb'    => 'bootstrap_navwalker::fallback', // For menu fallback
					) );
				?>
				</div>
			</div>
			<!--
			<div class="collapse navbar-collapse" id="collapsibleNavbar">
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="nav-link nav-" href="#">Inicio</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Link</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Link</a>
					</li>
				</ul>
			</div>-->
		</div>
	</nav> 
