<?php get_header(); ?>

<?php $headerLogo = get_field('logo_header', 'option'); ?>

<header class="section-header-logo">
  <div class="container">
    <div class="row">
      <div class="col-8 d-flex align-items-center justify-content-start">
        <div class="section-header-logo-image img-fluid" style="background-image: url('<?php echo $headerLogo['url']; ?>')"></div>
      </div>
      <div class="col-4 d-flex align-items-center justify-content-end">
        <?php if(get_field('whastapp_number', 'option')): ?>
          <a class="btn btn-whatsapp" target="_blank" aria-label="Whatsapp" href="https://wa.me/<?php echo get_field('whastapp_number', 'option'); ?>"><span class="d-none d-md-block">Whatsapp</span> <i class="fa fa-whatsapp"></i></a>       
        <?php endif; ?>
      </div>
    </div>
    <hr>
  </div>
</header>

<section class="content-section-landing default-text">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <?php
          if( have_posts() ): the_post();

              // Load default block template page
              the_content();

          endif;
        ?>

      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>
