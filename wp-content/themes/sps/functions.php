<?php
// Include your functions files here
include('inc/enqueues.php');
include('inc/excerpt.php');

// Register Nav Walker class
require get_template_directory() . '/bootstrap-navwalker.php';

//Scripts and Styles

function sps_scripts_styles(){

    // Normalize
    wp_enqueue_style('normalize', get_template_directory_uri().'/css/normalize.css', array(), '8.0.1');

    // Fontawesome
    wp_enqueue_style( 'fontawesome', 'https://use.fontawesome.com/releases/v5.3.1/css/all.css' );

    // SlickNav css
    wp_enqueue_style('slicknav_css', get_template_directory_uri().'/css/slicknav.min.css', array(), '1.0.0');

    // Bootstrap 
    wp_enqueue_style( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css');

    // Principal Style
    wp_enqueue_style('style', get_stylesheet_uri(), array('normalize'), '1.0.0');

    wp_enqueue_script('jquery');
    
    // Bootstrap
    wp_enqueue_script( 'popper_js', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js', array(), '1.16.0', true); 
    
    wp_enqueue_script( 'bootstrap_js', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js', array('jquery','popper_js'), '4.5.2', true);
    
    // SlickNav js
    wp_enqueue_script('slicknav_js', get_template_directory_uri().'/js/jquery.slicknav.min.js', array('jquery'), '1.0.0', true);

    // Custom Script
    wp_enqueue_script('scripts', get_template_directory_uri().'/js/scripts.js', array('jquery', 'slicknav_js'), '1.0.0', true);

    // Top Button
    wp_enqueue_script('top-button-script', get_stylesheet_directory_uri() . '/js/topbutton.js', array( 'jquery' )
  );

}
add_action( 'wp_enqueue_scripts', 'sps_scripts_styles');




/**
* Don't hesitate to use the WP code snippet generator Hasty : https://www.wp-hasty.com/
*/

 /**
  * Declare theme support
  * ( cf :  http://codex.wordpress.org/Function_Reference/add_theme_support )
  */
function theme_set_theme_supports() {
    global $wp_version;

    add_theme_support( 'menus' );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'theme_set_theme_supports' );



/**
 * Declare theme width global var
 */
if( !isset( $content_width ) ) {
    $content_width = 1170; // Bootstrap default container value
}


/**
 * Register WordPress menus
 * cf : http://codex.wordpress.org/Function_Reference/wp_nav_menu
 *
 */
register_nav_menus( array(
    'main_menu' => __( 'Main menu', 'bsbt' )
) );


/**
 * register sidebars
 * cf : https://codex.wordpress.org/Function_Reference/register_sidebar
 *
 * @return void
 */
function theme_register_sidebars() {
    if( !function_exists( 'register_sidebar' ) ) {
        return;
    }
    register_sidebar( array(
        'name' => __('Main sidebar', 'bsbt'),
        'id' => 'main-sidebar',
        'description' => '',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );
}
add_action( 'widgets_init', 'theme_register_sidebars' );


/**
 * Set style.css as style in admin editor
 *
 */
function theme_set_editor_style() {
    add_editor_style( get_stylesheet_directory_uri() . '/dist/css/theme.css' );
}

function cc_mime_types($mimes)
{
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/**
 * Remove emojis CSS and JS
 */
function theme_remove_emojis()
{
    // all actions related to emojis
    remove_action('admin_print_styles', 'print_emoji_styles');
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
    remove_filter('the_content_feed', 'wp_staticize_emoji');
    remove_filter('comment_text_rss', 'wp_staticize_emoji');

    // filter to remove TinyMCE emojis
    add_filter('tiny_mce_plugins', 'disable_emojicons_tinymce');
}


if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title'    => 'Website Settings',
        'menu_title'    => 'Website Settings',
        'menu_slug'     => 'website-general-settings',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));
}