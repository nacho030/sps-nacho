<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'sps_db2020' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ' /c]ZCe[f,kyLO~& bZ*c_cpPR[G7SW5J4:1Fdpc;^P@{ 23CV^]xW+IwCUEBK5Z' );
define( 'SECURE_AUTH_KEY',  '*tJu|bwiT.Y,}d:be7/*3o8t?ErPunu?[[c_}QU]F=9Kz+_DxxzvSJbuA3`IfvQA' );
define( 'LOGGED_IN_KEY',    'V=[{A@Ly+>u+n]:e`u@#w+_6{x;d|>1M&Hl/cnM({E%f30Gy{tX^xQ$!-!&/&2zG' );
define( 'NONCE_KEY',        'q^&LI$RNr7GZcF8y$UErxPE(c+isYW=P>p|wlM[;h4On8Ix@!7;a3K;I];S_EI&~' );
define( 'AUTH_SALT',        '2k3xMg7`6FroXMxmd298|}UZB4DiXLXyN|5$6zJY9dqj2R,C6#IgM]?AAU>Q8>Sl' );
define( 'SECURE_AUTH_SALT', 'fGZ+zLxG.1<AGkPArH^U?BKe2n-6bW}OafG+6@7tpgu*o:-ie9`N#v6K=f8jf:OK' );
define( 'LOGGED_IN_SALT',   'vzJ$N[*NwTV^E~rnG~YmLqbHXriLM#b|O|DdJ}RN^Ygzuwl;@Zl=KEJkw|*+Tn6z' );
define( 'NONCE_SALT',       'X[7(71.waZHFv@vU3T+/ps]AEZt5]$I_lMf-F.Xc/#0M_?!]PEuDZ:VOLg L]p V' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'sps_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
